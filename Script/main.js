/**
 * Generated from the Phaser Sandbox
 *
 * //phaser.io/sandbox/ZEfwFbib
 *
 * This source requires Phaser 2.6.2
 */

var game = new Phaser.Game(1900, 800, Phaser.AUTO, 'game', {
    preload: preload,
    create: create,
    update: update,
    render: render
});

function preload() {

    game.load.image('dx', './sprites/playerrx.png');
    game.load.image('background', './sprites/background.jpg');
    game.load.image('zomb', './sprites/zombie-1.png');
    game.load.image('m4', '/sprites/m4.png');
    game.load.image('bullet', './sprites/bullet.png');
    game.load.image('buyMaxHealth','./sprites/buyMaxHealth.png');
    game.load.image('buyRestoreHealth','./sprites/buyRestoreHealth.png');
    game.load.image('buySpeed','./sprites/buySpeed.png');
    game.load.image('buydmg','./sprites/buyDmg.png');

}

var player, platforms, cursors, jumpButton;
var zombiearray = [];
var zombo;

var bullets;
var sprite;
var fireRate = 150;
var nextFire = 0;
var m4dmg = 15;
var spawned = 0;
var spawnRate = 3;
var barConfig = {
    x: 150,
    y: 50,
    height: 30
};
var myHealthBar;
var playerMoney = 0;
var moneyText;
var score = 0;
var scoreText;

function create() {

    game.add.tileSprite(0, 0, 1900, 800, 'background');
    myHealthBar = new HealthBar(this.game, barConfig);
    myHealthBar.setBarColor('#cc0000');

    button = game.add.button(430, 28, 'buyRestoreHealth', buyHealth,this);
    button = game.add.button(660, 28, 'buyMaxHealth', buyMaxHealth,this);
    button = game.add.button(865, 28, 'buySpeed', buySpeed,this);
    button = game.add.button(1040, 28, 'buydmg', buyDmg,this);

    moneyText = game.add.text(290, 38, "Money : "+playerMoney,{ fontSize: '22px', fill: '#ffffff' });
    scoreText = game.add.text(1500, 38, "Score : " + score,{ fontSize: '22px', fill: '#ffffff' });

    bullets = game.add.group();
    bullets.enableBody = true;
    bullets.physicsBodyType = Phaser.Physics.ARCADE;

    bullets.createMultiple(50, 'bullet');
    bullets.setAll('checkWorldBounds', true);
    bullets.setAll('outOfBoundsKill', true);
    bullets.enableBody = true;

    player = game.add.sprite(100, 200, 'dx');
    player.health = 300;
    player.maxHealth = 300;
    player.speed = 0;
    player.anchor.setTo(.5, .5);

    game.physics.arcade.enable(player);

    player.body.collideWorldBounds = true;
    player.body.gravity.y = 2000;

    cursors = game.input.keyboard.createCursorKeys();
    jumpButton = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);

    //loop per generazione dei nemici 
    game.time.events.loop(Phaser.Timer.SECOND * spawnRate, generateZombie, this);

    //evento per quando muore il giocatore
    player.events.onKilled.add(function () {
        console.log("/");
    });

}

function update() {

    player.body.velocity.x = 0;

    if (cursors.left.isDown) {
        if (player.scale.x > 0) {
            player.scale.x *= -1;
        }
        player.body.velocity.x = -300 + player.speed * -1;
    } else if (cursors.right.isDown) {
        if (player.scale.x < 0) {
            player.scale.x *= -1;
        }
        player.body.velocity.x = 300 + player.speed;
    }

    if (cursors.up.isDown && (player.body.onFloor() || player.body.touching.down)) {
        player.body.velocity.y = -850;
    }
    if (game.input.activePointer.isDown) {
        fire();
    }

    follow();

}

function render() {

}

//Sparare
function fire() {
    if (game.time.now > nextFire && bullets.countDead() > 0 && player.alive) {
        nextFire = game.time.now + fireRate;

        var bullet = bullets.getFirstDead();

        bullet.reset(player.x - 8, player.y - 8);

        bullet.body.rotation = game.physics.arcade.angleToPointer(bullet);
        bullet.angle = (180 / Math.PI) * bullet.body.rotation;
        game.physics.arcade.moveToPointer(bullet, 1200);
    }

}


// Spostamento Zombie
function follow() {
    zombiearray.forEach((z) => {

        game.physics.arcade.collide(player, z, dmgp, null, this);
        game.physics.arcade.collide(z, bullets, kia, null, this);

        if (player.x - 60 < z.x && player.x + 60 > z.x) {
            z.body.velocity.x = 0;
        } else if (player.x < z.x) {
            z.body.velocity.x = -130 - z.speed;
            z.scale.x = -1;
        } else if (player.x > z.x) {
            z.body.velocity.x = 130 + z.speed;
            z.scale.x = 1;
        }

        z.healthBar.setPosition(z.x, z.y - 100);
    });
}


// Danni x Zombie
function kia(z, bullet) {
    if (z.health <= 15) {
        z.kill();
        z.healthBar.kill();
        playerMoney += z.money;
        score++;
    } else {
        z.damage(m4dmg);
        z.healthBar.setPercent((z.health / z.maxHealth) * 100);
    }
    bullet.kill();
    refreshText();
}

//Danni x Player
function dmgp(player, z) {

    if (game.time.now > z.nextattack) {
        z.nextattack = game.time.now + z.firerate;
        player.damage(z.attack);
        myHealthBar.setPercent((player.health/player.maxHealth)*100); //update vita giocatore
    }


    if (player.health <= 0) {
        player.kill();
    }

}

//generazione dei Zombie
function generateZombie() {

    for (let index = 0; index < 2; index++) {

        zombo = game.add.sprite(game.rnd.integerInRange(10, 1800), 400, 'zomb');

        zombo.anchor.setTo(0.5, 0.5);

        game.physics.arcade.enable(zombo);

        zombo.body.collideWorldBounds = true;
        zombo.body.gravity.y = 2000;
        zombo.health = 100 + game.rnd.integerInRange(0, 50);
        zombo.maxHealth = zombo.health;
        zombo.attack = 5;
        zombo.money = game.rnd.integerInRange(10, 20);
        zombo.nextattack = 0;
        zombo.firerate = 300;
        zombo.speed = Math.floor(game.rnd.integerInRange(10, 20));
        zombo.healthBar = new HealthBar(this.game, {
            x: zombo.x,
            y: zombo.y,
            width: 70,
            height: 10,
            bar: {
                color: '#9a0000'
            }
        });

        zombiearray.push(zombo);
    }

    if (spawnRate > 1) {
        spawnRate -= 0.1;
    }

}

function refreshText(){
    moneyText.setText('Money : '+ playerMoney); //renderizza testo soldi
    scoreText.setText('Score : '+ score); //renderizza testo punti
}


//funzioni per l'acquisto

function buyHealth() {
    if (playerMoney >= 300) {
        player.health = player.maxHealth;
        playerMoney -= 300;
    }
    refreshText();
}

function buyMaxHealth() {
    if (playerMoney >= 300) {
        player.maxHealth += 20;
        playerMoney -= 300;
    }
    refreshText();
    
}
function buySpeed() {
    if (playerMoney >= 200) {
        player.speed += 20;
        playerMoney -= 200;
    }
    refreshText();
    
}
function buyDmg() {
    if (playerMoney >= 200) {
        m4dmg += 0.5;
        playerMoney -= 200;
    }
    refreshText(); 
}